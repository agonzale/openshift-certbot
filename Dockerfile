FROM gitlab-registry.cern.ch/paas-tools/openshift-client

MAINTAINER Alvaro Gonzalez <Alvaro.Gonzalez@cern.ch>

RUN yum install -y certbot nginx && \
    yum clean all && \
    mkdir -p /var/lib/nginx/run && \
    chmod a+rwx -R /var/lib/nginx/ && \
    chmod a+rwx -R  /usr/share/nginx/html/ && \
    chmod o+rwx -R /var/log/nginx/

COPY nginx.conf /etc/nginx/
COPY bin/ /usr/local/bin/

EXPOSE 8080

ENTRYPOINT ["nginx"]
