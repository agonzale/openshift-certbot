![Let's Encrypt](le-logo-standard.png)

# Update/patch a route

```
key=$(sed ':a;N;$!ba;s/\n/\\n/g' privkey.pem)
oc patch route django-example -p "{\"spec\":{\"tls\":{\"key\":\""$key"\"}}}"
```

# Create template
```
oc export pvc,dc,svc,route,configmap,serviceaccount,scheduledjobs -o yaml --as-template=certbot > certbot.yaml
```
