#!/bin/bash

ROUTE_NAME=$1
EMAIL_CERT=$2

if [ -z "$EMAIL_CERT" ];
then
    echo "Use: $0 <ROUTE_NAME> <EMAIL_CERT>" >&2
    exit 1
fi

CERTIFICATE_FQDN=$(oc get "route/$ROUTE_NAME" -o go-template='{{.spec.host}}')

if [ -z $CERTIFICATE_FQDN ];
then
    echo -e "#\nCannot get the CERTIFICATE FQDN from the route, exiting\n" 2>/dev/null
    echo -e "#\n# In order to give permissions you can run something like:" 2>/dev/null
    echo -e "#\noc adm policy add-role-to-user edit -z certbot" 2>/dev/null
    exit 1
else
    echo "CERTIFICATE FQDN: $CERTIFICATE_FQDN."; echo
fi

CERTF="/etc/letsencrypt/live/$CERTIFICATE_FQDN/cert.pem"
CACERTF="/etc/letsencrypt/live/$CERTIFICATE_FQDN/chain.pem"
KEYF="/etc/letsencrypt/live/$CERTIFICATE_FQDN/privkey.pem"

# This file is to easy the code to detect new certificates
touch /tmp/before

mkdir -p /etc/letsencrypt/html

# If the certificate is not yet there, make a request
if [ ! -f "$CERTF" ];
then
    echo "Requesting certificate."; echo

    certbot certonly \
        --config-dir /etc/letsencrypt/ \
        --work-dir /etc/letsencrypt/ \
        --logs-dir /etc/letsencrypt/ \
        --webroot \
        --webroot-path /etc/letsencrypt/html \
        --agree-tos \
        -m "$EMAIL_CERT" \
        -d "$CERTIFICATE_FQDN" \
        -n
    if [ $? -ne 0 ];
    then
        echo -e "#\nERROR: Cannot request the certificate\n#"
        exit 2
    fi

# Otherwise try to renew it.
# This will try to renew all configured certificates.
else
    echo "Renewing certificate."; echo

    certbot renew \
        --config-dir /etc/letsencrypt/ \
        --work-dir /etc/letsencrypt/ \
        --logs-dir /etc/letsencrypt/ \
        --webroot-path /etc/letsencrypt/html
    if [ $? -ne 0 ];
    then
        echo -e "#\nERROR: Cannot renew the certificate\n#"
        exit 3
    fi
fi
#
# Must expand with other cases not covered (Certificate expired long time ago(?))
#

#
# Only if new certificate
#
if [ /tmp/before -ot "$CERTF" ];
then
    patch=$(jq -n --arg newcert "$(cat $CERTF)" --arg newkey "$(cat $KEYF)" \
        --arg newcaCertificate "$(cat $CACERTF)" \
        '{"spec":{"tls":{"certificate":$newcert, "key": $newkey, "caCertificate": $newcaCertificate}}}')
    oc patch "route/$ROUTE_NAME" -p "$patch"
    if [ $? -ne 0 ];
    then
        echo "ERROR: while doing: oc patch route/$ROUTE_NAME -p \"$patch\""
    fi
else
    echo "Certificate file ($CERTF) has not changed"
fi

openssl x509 -in "/etc/letsencrypt/live/$CERTIFICATE_FQDN/cert.pem" -noout -dates

openssl x509 -in "/etc/letsencrypt/live/$CERTIFICATE_FQDN/cert.pem" -noout -checkend $((30*24*60*60))
if [ $? -ne 0 ];
then
    openssl x509 -in "/etc/letsencrypt/live/$CERTIFICATE_FQDN/cert.pem" -noout -dates
    echo "Certificate will expire in less than a month, certbot should had renew it!" >&2
    exit 1
fi

exit 0
